<?php

/**
 * hook_menu() page callback for 'node/%node/search_dataset'
 */
function raw_search_data_dataset($node) {
  $query = db_select('search_dataset', 'd');
  
  $query->condition('d.sid', $node->nid)
    ->condition('d.type', 'node')
    ->fields('d', array('data'));
    
  $data = $query->execute()->fetchField();

  $dirty = db_query("SELECT reindex FROM {search_dataset} WHERE sid = :sid AND type = 'node'",
      array(':sid' => $node->nid))->fetchField() != 0;

  $build = array(
    '#theme' => 'raw_search_data_dataset',
    '#node' => $node,
    '#data' => check_plain($data),
    '#dirty' => $dirty,
  );

  return $build;
}

/**
 * hook_menu() page callback for 'node/%node/search_index'
 */
function raw_search_data_index($node) {
  $header = array();
    
  $header[] = array(
    'data' => t('word'),
    'field' => 's.word',
  );
  
  $header[] = array(
    'data' => t('score'),
    'field' => 's.score',
    'sort' => 'desc',
  );
    
  $query = db_select('search_index', 's')->extend('TableSort');
  
  $query->condition('s.sid', $node->nid)
    ->condition('s.type', 'node')
    ->fields('s', array('word', 'score'));
    
  $result = $query
    ->orderByHeader($header)
    ->execute();

  $dirty = db_query("SELECT reindex FROM {search_dataset} WHERE sid = :sid AND type = 'node'",
    array(':sid' => $node->nid))->fetchField() != 0;

  $rows = array();

  foreach ($result as $row) {
    $rows[] = array(check_plain($row->word), $row->score);
  }

  $build = array(
    '#theme' => 'raw_search_data_index',
    '#node' => $node,
    '#index' => array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ),
    '#dirty' => $dirty,
  );

  return $build;
}

/**
 * hook_menu() page callback for 'node/%node/deindex'
 */
function raw_search_data_deindex($node) {
  search_reindex($node->nid, 'node');

  drupal_set_message(t('Deleted search index data for node/@nid', array('@nid' => $node->nid)));

  drupal_goto('node/' . $node->nid);
}

/**
 * hook_menu() page callback for 'node/%node/reindex'
 */
function raw_search_data_reindex($node) {
  // duplicate code from _node_index_node(), but without the
  // variable_set('node_cron_last', $node->changed) so we don't disturb the half-life calculations

  $build = node_view($node, 'search_index');
  unset($build['#theme']);
  $node->rendered = drupal_render($build);

  $text = '<h1>' . check_plain($node->title) . '</h1>' . $node->rendered;

  $extra = module_invoke_all('node_update_index', $node);
  foreach ($extra as $t) {
    $text .= $t;
  }

  search_index($node->nid, 'node', $text);

  drupal_set_message(t('Recreated search index data for node/@nid', array('@nid' => $node->nid)));

  drupal_goto('node/' . $node->nid);
}

/**
 * hook_menu() page callback for 'node/%node/mark_dirty'
 */
function raw_search_data_mark_dirty($node) {
  search_touch_node($node->nid);

  drupal_set_message(t('Marked node/@nid as dirty for search indexing', array('@nid' => $node->nid)));

  drupal_goto('node/' . $node->nid);
}

/**
 * Process variables for raw-search-data-dataset.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $node: The node being displayed.
 * - $data: The raw search data.
 * - $dirty: TRUE if search data is out of date, and the node needs to be reindexed.
 *
 * @see raw-search-data-dataset.tpl.php
 */
function template_preprocess_raw_search_data_dataset(&$variables) {
}

/**
 * Process variables for raw-search-data-index.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $node: The node being displayed.
 * - $index: The search index data, as a render array.
 * - $dirty: TRUE if search data is out of date, and the node needs to be reindexed.
 *
 * @see raw-search-data-index.tpl.php
 */
function template_preprocess_raw_search_data_index(&$variables) {
}