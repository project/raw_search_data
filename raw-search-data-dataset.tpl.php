<?php

/**
 * @file
 * Default theme implementation for raw search data from {search_dataset}.
 *
 * Available variables:
 * - $node: The node being displayed.
 * - $data: The raw search data.
 * - $dirty: TRUE if search data is out of date, and the node needs to be reindexed.
 *
 * @see template_preprocess()
 * @see template_preprocess_raw_search_data_dataset()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="raw-search-data-dataset">
  <?php print $data; ?>
</div>
