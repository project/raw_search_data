<?php

/**
 * Implements hook_help().
 */
function raw_search_data_help($path, $arg) {
  switch ($path) {
    case 'admin/help#raw_search_data':
      return _raw_search_data_get_readme();
  }
}

function _raw_search_data_get_readme() {
  $readme = file_get_contents(dirname(__FILE__) . '/README.txt');

  if (module_exists('markdown')) {
    $filters = module_invoke('markdown', 'filter_info');
    $info = $filters['filter_markdown'];

    if (function_exists($info['process callback'])) {
      $function = $info['process callback'];
      $output = filter_xss_admin($function($readme, NULL));
    }
    else {
      $output = '<pre>'. check_plain($readme) .'</pre>';
    }
  }
  else {
    $output = '<pre>'. check_plain($readme) .'</pre>';
  }

  return $output;
}

/**
 * Implements hook_permission().
 */
function raw_search_data_permission() {
  return array(
    'view raw search dataset' => array(
      'title' => t('View raw search dataset'),
      'description' => t('View the raw search dataset for a node.'),
    ),
    'view raw search index' => array(
      'title' => t('View raw search index'),
      'description' => t('View the raw search index for a node.'),
    ),
    'force node deindex' => array(
      'title' => t('Force node deindexing'),
      'description' => t('Temporarily remove a node from the search index.'),
    ),
    'force node reindex' => array(
      'title' => t('Force node reindexing'),
      'description' => t('Immediately rebuild index data for a node.'),
    ),
    'force node as dirty' => array(
      'title' => t('Force node as dirty'),
      'description' => t('Mark a node as needing to be reindexed.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function raw_search_data_menu() {
  $items = array();
  
  $items['node/%node/search_dataset'] = array(
    'title' => 'Search Data',
    'page callback' => 'raw_search_data_dataset',
    'page arguments' => array(1),
    'access callback' => 'raw_search_data_access',
    'access arguments' => array(1, 'view raw search dataset', 'view'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'raw_search_data.pages.inc',
    'weight' => 100,
  );
  
  $items['node/%node/search_index'] = array(
    'title' => 'Search Index',
    'page callback' => 'raw_search_data_index',
    'page arguments' => array(1),
    'access callback' => 'raw_search_data_access',
    'access arguments' => array(1, 'view raw search index', 'view'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'raw_search_data.pages.inc',
    'weight' => 100,
  );

  $items['node/%node/deindex'] = array(
    'title' => 'Deindex',
    'page callback' => 'raw_search_data_deindex',
    'page arguments' => array(1),
    'access callback' => 'raw_search_data_access',
    'access arguments' => array(1, 'force node deindex', 'update'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'raw_search_data.pages.inc',
  );

  $items['node/%node/reindex'] = array(
    'title' => 'Reindex',
    'page callback' => 'raw_search_data_reindex',
    'page arguments' => array(1),
    'access callback' => 'raw_search_data_access',
    'access arguments' => array(1, 'force node reindex', 'update'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'raw_search_data.pages.inc',
  );

  $items['node/%node/mark_dirty'] = array(
    'title' => 'Mark Dirty',
    'page callback' => 'raw_search_data_mark_dirty',
    'page arguments' => array(1),
    'access callback' => 'raw_search_data_access',
    'access arguments' => array(1, 'force node as dirty', 'update'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'raw_search_data.pages.inc',
  );

  return $items;
}

/**
 * Common access callback for raw_search_data_menu().
 */
function raw_search_data_access($node, $permission, $op) {
  if (!$node) {
    return FALSE;
  }
  else if ($node->status != NODE_PUBLISHED) {
    return FALSE;
  }
  else if (!user_access($permission)) {
    return FALSE;
  }
  else {
    return node_access($op, $node);
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 *
 * See http://drupal.stackexchange.com/a/110684
 */
function raw_search_data_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  if ($root_path == 'node/%/search_dataset' || $root_path == 'node/%/search_index') {
    $nid = arg(1);

    foreach (array('deindex', 'reindex', 'mark_dirty') as $local) {
      $item = menu_get_item('node/' . $nid . '/' . $local);

      if ($item['access']) {
        $data['actions']['output'][] = array(
          '#theme' => 'menu_local_action',
          '#link' => $item,
        );
      }
    }
  }
}

/**
 * Implements hook_theme().
 */
function raw_search_data_theme($existing, $type, $theme, $path) {
  return array(
    'raw_search_data_dataset' => array(
      'variables' => array(
        'node' => NULL,
        'data' => NULL,
        'dirty' => NULL,
      ),
      'file' => 'raw_search_data.pages.inc',
      'template' => 'raw-search-data-dataset',
    ),
    'raw_search_data_index' => array(
      'variables' => array(
        'node' => NULL,
        'index' => NULL,
        'dirty' => NULL,
      ),
      'file' => 'raw_search_data.pages.inc',
      'template' => 'raw-search-data-index',
    ),
  );
}
