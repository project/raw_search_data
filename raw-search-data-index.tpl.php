<?php

/**
 * @file
 * Default theme implementation for raw index data from {search_index}.
 *
 * Available variables:
 * - $node: The node being displayed.
 * - $index: The search index data, as a render array.
 * - $dirty: TRUE if search data is out of date, and the node needs to be reindexed.
 *
 * @see template_preprocess()
 * @see template_preprocess_raw_search_data_index()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="raw-search-data-index">
  <?php print render($index); ?>
</div>
