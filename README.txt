Raw Search Data
===============

This is a small module that adds tabs to node pages for viewing raw
information from the core Search module.  It also defines some local actions
for altering the search index for an individual node.

The official project home is https://drupal.org/project/raw_search_data

Search Data
----------- 

This tab will display the version of the node that the Search module has 
indexed. It is essentially a plain text version of the 'search_index'
display mode for the node, with all of the additions that other modules
have appended. This can be useful for debugging why a particular node 
appears (or doesn't appear) in a results listing. 

Users will see this tab if they have the 'View raw search dataset' 
permission, and they have node_access view rights for the node.

This output is themeable through the 'raw_search_data' hook.  The
$variables array will contain a single key, 'data', which is the data
to be displayed.

Search Index 
------------ 

This tab queries the search database and constructs a sortable table with
the indexed words and their score for that particular node. A higher 
score means signifies higher importance. 

Users will see this tab if they have the 'View raw search index' 
permission, and they have node_access view rights for the node.

Local Actions
-------------

Deindex: This will temporarily remove a node from the search index.  The node
will be indexed according to the site's normal schedule.

Reindex: This will force the index for a particular node to be rebuilt
immediately, instead of waiting for a cron run.

Mark Dirty: This will mark a node as needing to be reindexed according to the
site's normal schedule.  Under normal circumstances with proper hook
implementations, this should never need to be done.

Installation
------------ 

Download and install the module using the normal methods. There is
a hard dependency on the core Search module, so that needs to be 
enabled, too. 

After installing, visit the permissions page and assign the permissions to the
roles that need to debug searches.  Note that the Search Data and Search Index
also require a user to have "View" permission for the node.  The local actions
also require users to have "Update" permissions for the node.

Problems and Feedback
---------------------

If you encounter any problems, have any questions, or think of a feature
that would benefit this module, please create an issue at
https://drupal.org/project/issues/raw_search_data
